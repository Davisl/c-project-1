#include<iostream>
#include<map>
using namespace std;
int main()
{
	map<int, string>mp;

	mp.insert(pair<int, string>(123456790, "Paul Brown"));
	mp.insert(pair<int, string>(123456791, "Mary Smith"));
	mp.insert(pair<int, string>(123456789, "John Smith"));
	mp.insert(pair<int, string>(123456792, "Lisa Brown"));
	map<int, string>::iterator itr;

	cout << "\n part 1 retrieve all names:\n";
		for (itr = mp.begin(); itr != mp.end(); ++itr) {
			cout << itr->first << '\t' << itr->second << endl;
		}

		cout << "\n Part 2 Searching \n";
		for (itr = mp.begin(); itr != mp.end(); ++itr) {
			if (itr->second == "Paul Brown")
				cout << itr->second << " found" << '\t' << itr->first << endl;

		}

		for (itr = mp.begin(); itr != mp.end(); ++itr) {
			if (itr->second == "John Brown")
				cout << itr->second << " found" << '\t' << itr->first << endl;

		}

		if (itr == mp.end())
			cout << "John Brown not found";
		
		return 0;

		system("pause");
}